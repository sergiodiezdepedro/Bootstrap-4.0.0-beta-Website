# Layout con Bootstrap 4

Incluye un entorno de desarrollo de Bootstrap 4.0.0-beta con Gulp y Sass.

## Instalar Dependencias

```bash
npm install
```

## Compilar Sass & y Ejecutar el Servidor de Desarrollo

```bash
npm start
```
Los archivos se compilan en la carpeta /src
